﻿using UnityEngine;
using UnityEditor;

public static class DebugMenu {
    [MenuItem("Debug/Print Global Position")]
    public static void PrintGlobalPosition () {
        if (Selection.activeGameObject != null) {
            Debug.Log(Selection.activeGameObject.name + " is at " + Selection.activeGameObject.transform.position);
        }
    }

    [MenuItem("Debug/Print Camera Info")]
    public static void CameraInfo () {
        int count = Camera.allCameras.Length;
        Debug.Log("We've got " + count + " cameras");
//        Debug.Log("main:" + Camera.main.name);
        Debug.Log("current:" + Camera.current.name);
    }
}