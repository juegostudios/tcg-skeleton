﻿using UnityEngine;
using System.Collections;
using PlayFab;
using PlayFab.ClientModels;

public class LoginManager : MonoBehaviour {
    public AudioTransition audioHandler;
    public Login login;
    public Signup signup;
    public GameObject MainLayout;

    IEnumerator Start()
    {
        // If we're trying to validate the last ticket, then wait
        while( GameManager.IS_LOGGING_IN )
            yield return null;
        
        if(GameManager.PLAYFAB_USER != null) {
            Helper.Log ("Quick signing: " + GameManager.PLAYFAB_USER.PrivateInfo.Email);
            // Show Menu directly
 Application.LoadLevel( "GameStart" );
        } else {
            // Set admin as the user to track events
            GameManager.SetUserTracking("tech@silentlegends.com");
        }
    }

    public void ShowLogin()
    {
        login.gameObject.SetActive(true);
        signup.gameObject.SetActive(false);
        MainLayout.SetActive(false);
    }

    public void ShowSignUp()
    {
        login.gameObject.SetActive(false);
        signup.gameObject.SetActive(true);
        MainLayout.SetActive(false);
    }

    public void Cancel()
    {
        login.gameObject.SetActive(false);
        login.Clear(); 
        signup.gameObject.SetActive(false);
        signup.Clear();
        MainLayout.SetActive(true);
    }

    void CreateSLUser() {
        var signupRequest = new RegisterPlayFabUserRequest {
                    Email = Config.INVITE_CODE_VALIDATOR_EMAIL,
                    Password = Config.INVITE_CODE_VALIDATOR_PASSWORD,
                    RequireBothUsernameAndEmail = false,
                    DisplayName = "sl_invitecode_validator",
                    Username = "invitecodevalidator"
                };
                // Register the player
                PlayFabClientAPI.RegisterPlayFabUser( signupRequest
                    , result => {Debug.Log(result);}
                    , error => {Debug.Log(error);}
                );
        }
}
