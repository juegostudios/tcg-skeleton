using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class UserUtils : MonoBehaviour {
    public AudioTransition audioHandler;
    public void Logout() {
        // Discard the session ticket for the current user
        PlayFab.Internal.PlayFabHttp.SetAuthKey( null );
        // And delete the stored token from the player preferences so we don't
        // auto login next time
        PlayerPrefs.DeleteKey( GameManager.PREFKEY_LAST_SESSION_TICKET );
        GameManager.PLAYFAB_USER = null;
        StartCoroutine(audioHandler.FadeAndLoadNextLevel(audio, "AppStart"));
    }
}