using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using PlayFab;
using PlayFab.ClientModels;

public class Login : MonoBehaviour {
    public UILabel email;
    public UILabel password;
    public UILabel errorLabel;
    public UILabel emailErrorIndicator;
    public UILabel pwdErrorIndicator;
    public AudioTransition audioHandler;
    private string errorMessage;
    private bool isSuccess;
    public GameObject loaderPrefab;
    public GameObject msgWindowPrefab;
    public Color okLabelColor;

    GameObject mainRoot;
    Loading loader;
    void Start() {
        // Set reference to Main gameobject
        mainRoot = GameObject.Find("Main");
    }

    public void Submit () {
        // Take screename/email and validate 
        Helper.Log("on click");
        if (!isValid()) {
            return;
        }
        Helper.Log("checking values");
        // Show loading indicator
        loader = Helper.GetLoadingAnimation(mainRoot, loaderPrefab);
        loader.StartLoading();
        // Call API to check user
        var loginRequest = new LoginWithEmailAddressRequest {
            Email = email.text.ToLower(),
            Password = password.text,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams {
                GetUserAccountInfo = true
            }
        };

        PlayFabClientAPI.LoginWithEmailAddress( loginRequest
            , response => {
                    loader.StopLoading();
                    // Make sure they haven't cheated and used the dummy account to log in
                    if( GameManager.PLAYFAB_USER != null
                     && GameManager.PLAYFAB_USER.PrivateInfo.Email == Config.INVITE_CODE_VALIDATOR_EMAIL ) {
                        return; // Don't go to the next level
                    }
                    GameManager.SetPlayFabUser( response.InfoResultPayload.AccountInfo );
                    GameManager.StoreSession( response.SessionTicket );
                    // Load next screen
                    
                    Application.LoadLevel( "GameStart" );
                }
            , error => {
                    loader.StopLoading();
                    Helper.ShowPlayFabError(error, mainRoot, msgWindowPrefab);
                } );
    }
    
    bool isValid () {
        bool valid = true;
        bool isEmail = Regex.IsMatch(email.text, Config.EMAIL_REGEX);
        if (email.text == "" || !isEmail) {
            emailErrorIndicator.color = Color.red;
            valid = false;
        } else {
            emailErrorIndicator.color = okLabelColor;//Color.green;
        }
        if (password.text == "") {
            pwdErrorIndicator.color = Color.red;
            valid = false;
        } else {
            pwdErrorIndicator.color = okLabelColor;//Color.green;
        }
        return valid;
    }

    public void Clear() {
        email.text = "";
        password.text = "";
    }
}
