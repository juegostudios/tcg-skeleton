using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using PlayFab;
using PlayFab.Json;
using PlayFab.ClientModels;

public class Signup : MonoBehaviour {

    public UILabel email;
    public UILabel screenName;
    public UILabel password;
    public UIToggle termsOfA;

    public UILabel emailErrorIndicator;
    public UILabel pwdErrorIndicator;
    public UILabel screenNameErrorIndicator;
    public UISprite termsOfAErrorIndicator;

    public UILabel inviteCode;
    public UILabel inviteCodeErrorIndicator;
    public GameObject inviteCodeGameObject;

    public AudioTransition audioHandler;
    public GameObject loaderPrefab;
    public GameObject msgWindowPrefab;
    public Color okLabelColor;

    GameObject mainRoot;
    Loading loader;

    string validatedCode;

    void Start() {
        // Set reference to Main gameobject
        mainRoot = GameObject.Find("Main");
        // Show or hide inviteCode
        inviteCodeGameObject.SetActive(Config.INVITE_ONLY_SIGNUP);
        loader = Helper.GetLoadingAnimation(mainRoot, loaderPrefab);
        loader.StopLoading();
    }

    public void Submit () {
        // Take screename/email and validate
        if (!isValid()) {
            return;
        }
        Helper.Log("saving values");

        loader.StartLoading();

        if( Config.INVITE_ONLY_SIGNUP ) {
            // Log into a dummy account to validate the 
            var validatorLogin = new LoginWithEmailAddressRequest {
                Email = Config.INVITE_CODE_VALIDATOR_EMAIL,
                Password = Config.INVITE_CODE_VALIDATOR_PASSWORD
            };

            PlayFabClientAPI.LoginWithEmailAddress( validatorLogin
                , ValidateInviteCode
                , DisplayErrorMessage );
        } else {
            PerformSignup();
        }
    }

    void PerformSignup() {
        
        var signupRequest = new RegisterPlayFabUserRequest {
            Email = email.text.ToLower(),
            Password = password.text,
            RequireBothUsernameAndEmail = false,
            DisplayName = screenName.text
        };
        // Register the player
        PlayFabClientAPI.RegisterPlayFabUser( signupRequest
            , registration => {
                    GameManager.StoreSession( registration.SessionTicket );
                    
                    if( validatedCode != null ) {
                        var assocReq = new ExecuteCloudScriptRequest {
                            FunctionName = "associateInviteCode",
                            FunctionParameter = new {
                                inviteCode = validatedCode
                            }
                        };
                        PlayFabClientAPI.ExecuteCloudScript( assocReq
                            , reponse => {}
                            , err => Helper.ShowPlayFabError( err, mainRoot, msgWindowPrefab ) );
                    }

                    // Fetch their data
                    GameManager.FetchAndSetPlayFabUser( LoadNextLevel, DisplayErrorMessage );
                }
            , DisplayErrorMessage );
    }

    void LoadNextLevel() {
        loader.StopLoading();

        // Make sure they haven't cheated and used the dummy account to log in
        if( GameManager.PLAYFAB_USER.PrivateInfo.Email == Config.INVITE_CODE_VALIDATOR_EMAIL ) {
            return; // Don't go to the next level
        }

        // Load next screen
        GameManager.IS_NEW_USER = true;
 		Application.LoadLevel( "GameStart" );
        
    }

    void ValidateInviteCode( LoginResult validatorLogin ) {
        // Just go straight to the signup if the code has been validated
        if( !string.IsNullOrEmpty( validatedCode ) && inviteCode.text == validatedCode ) {
            PerformSignup();
            return;
        }
        
        var validationRequest = new ExecuteCloudScriptRequest {
            FunctionName = "validateInviteCode",
            FunctionParameter = new { inviteCode = inviteCode.text }
        };
        
        PlayFabClientAPI.ExecuteCloudScript( validationRequest
            , result => {
                    var data = (JsonObject) result.FunctionResult;
                    
                    // Check if the validation succeeded
                    if( (bool)data["succeeded"] ) {
                        Helper.Log( "Invite code validated" );
                        GA.API.Error.NewEvent(GA_Error.SeverityType.warning, "Parse:UpdateInviteCode:" + inviteCode.text);
                        // Store that we've validated the code, so that if the 
                        // signup fails, we don't have to validate the code again
                        validatedCode = inviteCode.text;
                        PerformSignup();
                    } else {
                        loader.StopLoading();
                        var msg = (string)((JsonObject) data["error"])["message"];
                        Helper.ShowMessage(mainRoot, msgWindowPrefab, msg);
                    }
                }
            , DisplayErrorMessage );
    }

    void DisplayErrorMessage( PlayFabError err ) {
        loader.StopLoading();
        Helper.ShowPlayFabError( err, mainRoot, msgWindowPrefab );
    }

    bool isValid () {
        bool valid = true;
        // Set acceptable email pattern
        bool isEmail = Regex.IsMatch(email.text, Config.EMAIL_REGEX);

        if (email.text == "" || !isEmail) {
            emailErrorIndicator.color = Color.red;
            valid = false;
        } else {
            emailErrorIndicator.color = okLabelColor;
        }
        if (screenName.text == "") {
            screenNameErrorIndicator.color = Color.red;
            valid = false;
        } else {
            screenNameErrorIndicator.color = okLabelColor;
        }
        if (password.text.Length < 6 || password.text.Length > 100) {
            pwdErrorIndicator.color = Color.red;
            valid = false;
        } else {
            pwdErrorIndicator.color = okLabelColor;
        }
        if(Config.INVITE_ONLY_SIGNUP) {
            if (inviteCode.text == "") {
                inviteCodeErrorIndicator.color = Color.red;
                valid = false;
            } else {
                inviteCodeErrorIndicator.color = okLabelColor;
            }
        }
        if (!termsOfA.value) {
            termsOfAErrorIndicator.color = Color.red;
            valid = false;
        } else {
            termsOfAErrorIndicator.color = okLabelColor;
            // Disable hover behavior that changes color again
            termsOfA.GetComponentInChildren<UIButton>().enabled = false;
        }
        return valid;
    }

    public void Clear() {
        email.text = "";
        password.text = "";
        screenName.text = "";
        inviteCode.text = "";
        //emailErrorIndicator.text = "";
        //pwdErrorIndicator.text = "";
        //screenNameErrorIndicator.text = "";
        //inviteCodeErrorIndicator.text = "";
    }
}
