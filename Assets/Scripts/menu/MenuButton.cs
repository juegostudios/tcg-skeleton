﻿using UnityEngine;
using System.Collections;

public class MenuButton : MonoBehaviour {
    public MenuHandler menuHandler;
    public string message;

    void OnPress (bool isPressed) {
        if(isPressed)
            menuHandler.OnButton(message);
    }
}
