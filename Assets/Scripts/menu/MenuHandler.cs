using UnityEngine;

public class MenuHandler : MonoBehaviour {
    public AudioTransition audioHandler;


    virtual public void OnButton (string msg) {
        switch (msg) {
        case "Demo":
            StartCoroutine(audioHandler.FadeAndLoadNextLevel(audio, "DemoAndroid"));
            break;
        case "Menu":
            StartCoroutine(audioHandler.FadeAndLoadNextLevel(audio, "GameStart"));
            SendMessage("OnBackToMenu", null, SendMessageOptions.DontRequireReceiver);
            break;
        case "Play":
            // Hack since there's no tutorial campaign anymore
            PlayerPrefs.SetInt(GameManager.PREFKEY_TUT_COMPLETED, 1);
            PlayerPrefs.Save();
            if (PlayerPrefs.GetInt(GameManager.PREFKEY_TUT_COMPLETED, 0) == 1) {
                StartCoroutine(audioHandler.FadeAndLoadNextLevel(audio, "PlaySelection"));
            } else {
                // First timer user still did not do Tutorial
                StartCoroutine(audioHandler.FadeAndLoadNextLevel(audio, "Tutorial Hands-On"));
            }
            break;
        case "Collection":
            StartCoroutine(audioHandler.FadeAndLoadNextLevel(audio, "DeckBuilder"));
            break;
        case "Quests":
            StartCoroutine(audioHandler.FadeAndLoadNextLevel(audio, "Quests"));
            break;
        case "Store":
            StartCoroutine(audioHandler.FadeAndLoadNextLevel(audio, "Store"));
            break;
        case "Tutorial":
            StartCoroutine(audioHandler.FadeAndLoadNextLevel(audio, "Tutorial Hands-On"));
            break;
        case "Quit":
            Application.Quit();
            break;
        }
    }
}
