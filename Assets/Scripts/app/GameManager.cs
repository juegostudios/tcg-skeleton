using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using LitJson;
using PlayFab;
using PlayFab.Json;
using PlayFab.ClientModels;

[System.Serializable]
public enum OpponentType : byte {
    RANDOM,
    COMPUTEREASY,
    COMPUTERMEDIUM,
    COMPUTERHARD}
;

[System.Serializable]
public enum GameMode : byte {
    REALTIME,
    TURNBASED,
    TUTORIAL,
    PVP_REALTIME}
    ;

[System.Serializable]
public enum PlayResult : byte {
    WIN,
    LOSS,
    DRAW}
;


public class GameManager {
    // PlayerPref keys BEGIN
    public static string PREFKEY_APP_OPENED;
    public static string PREFKEY_NEWS_SHOWN;
    public static string PREFKEY_NEWS_LAST_RELEASED;
    public static string PREFKEY_LAST_DOWNLOADED;
    public static string PREFKEY_TUT_COMPLETED;
    public static string PREFKEY_LAST_WINNING_LEVEL;
    public static string PREFKEY_TUT_LAST_Q_LEVEL;
    public static string PREFKEY_TUT_LAST_Q_WIN;
    public static string PREFKEY_LAST_SESSION_TICKET = GameManager.APPID + ":LastSessionTicket";
    public static string PREFKEY_CLAIMED_STARTER_BUNDLE;
    public static string PREFKEY_FIRST_TIME_DECK_BUILDER;
    // PlayerPref Keys END

    public static bool IS_LOGGING_IN;
    public static UserAccountInfo PLAYFAB_USER;
    public static string SCREENNAME;
public static bool IS_NEW_USER;
public static string APPNAME;
public static JsonData TUTORIAL_DATA;
public static string APPID = "DgBRWBP7rr";
    public static bool CONFIG_LOADED = false;

    public static void AttemptToLoadSessionTicket() {
        // Try to load the last session ticket to see if it's still valid
        if( PlayerPrefs.HasKey( PREFKEY_LAST_SESSION_TICKET ) ) {
            var sessionTicket = PlayerPrefs.GetString( PREFKEY_LAST_SESSION_TICKET );
            Helper.Log( "Last session: " + sessionTicket );
            PlayFab.Internal.PlayFabHttp.InitializeHttp();
            PlayFab.Internal.PlayFabHttp.SetAuthKey( sessionTicket );
            
            IS_LOGGING_IN = true;

            // Try to fetch our user data, if we fail assume that it's because
            // the ticket is expired and we need to log in again, therefore do
            // nothing when we get an error and just let the PLAYFAB_USER be null
            FetchAndSetPlayFabUser( ()  => IS_LOGGING_IN = false
                                  , err => IS_LOGGING_IN = false );
        }
    }

    public static void FetchAndSetPlayFabUser(Action callback, Action<PlayFabError> onError) {
        PlayFabClientAPI.GetAccountInfo( new GetAccountInfoRequest()
            , info => {
                    SetPlayFabUser(info.AccountInfo);
                    if( callback != null )
                        callback();
                }
            , onError );
    }

    public static void StoreSession(string ticket) {
        PlayerPrefs.SetString( PREFKEY_LAST_SESSION_TICKET, ticket );
        PlayerPrefs.Save();
    }

    public static void SetPlayFabUser(UserAccountInfo user) {
        PLAYFAB_USER = user;
        SetUserTracking( PLAYFAB_USER.PrivateInfo.Email.ToLower() );
    }

    /**
     * Use custom user tracking using SHA-encrypted email
     * to identify unique users. We dont want to multiple-count same user
     * if logged in through different devices
     */
    public static void SetUserTracking(string gaUserId) {

        SHA1CryptoServiceProvider SHA = new SHA1CryptoServiceProvider();
        byte[] authData = Encoding.UTF8.GetBytes(gaUserId);

        gaUserId = BitConverter.ToString(SHA.ComputeHash(authData)).Replace("-", "");
        GA.SettingsGA.SetCustomUserID(gaUserId);

        if(PLAYFAB_USER != null) {
            // Set screenname
            GameManager.SCREENNAME = PLAYFAB_USER.TitleInfo.DisplayName;

            Debug.Log (GameManager.SCREENNAME);
            // Update PREF Keys
            PREFKEY_APP_OPENED = GameManager.APPID + ":" + GameManager.SCREENNAME + ":LastOpened";
            PREFKEY_NEWS_SHOWN = GameManager.APPID + ":" + GameManager.SCREENNAME + ":UpdatesShown";
            PREFKEY_NEWS_LAST_RELEASED = GameManager.APPID + ":" + GameManager.SCREENNAME + ":LastReleased";
            PREFKEY_LAST_DOWNLOADED = GameManager.APPID + ":" + "LastDownloaded";
            PREFKEY_TUT_COMPLETED = GameManager.APPID + ":" + GameManager.SCREENNAME + ":COMPLETEDTUT";
            PREFKEY_LAST_WINNING_LEVEL = GameManager.APPID + ":" + GameManager.SCREENNAME + ":LASTWINNINGLEVEL";
            PREFKEY_TUT_LAST_Q_LEVEL = GameManager.APPID + ":" + GameManager.SCREENNAME + ":LASTTUTQLEVEL";
            PREFKEY_TUT_LAST_Q_WIN = GameManager.APPID + ":" + GameManager.SCREENNAME + ":LASTTUTQWIN";
            PREFKEY_CLAIMED_STARTER_BUNDLE = GameManager.APPID + ":" + GameManager.SCREENNAME + ":CLAIMEDSTARTERBUNDLE";
            PREFKEY_FIRST_TIME_DECK_BUILDER = GameManager.APPID + ":" + GameManager.SCREENNAME + ":FIRSTTIMEDECKBUILDER";
        }
    }
}
