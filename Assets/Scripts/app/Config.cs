using UnityEngine;
using System.Collections.Generic;

public class Config {
    public const string EMAIL_REGEX =
        @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
        + "@"
        + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$";

    /** API KEYS **/
    public const string API_URL = "http://sl-citadel.herokuapp.com";
    public const string API_PUBLIC_KEY = "silentLegends";
    public const string API_PRIVATE_KEY = "citadel";

    /** ACHIEVEMENTS **/
    public const string ACHV_JOIN_KEY = "justjoined";
    public const string ACHV_JOIN_NAME = "Just Joined";

    /** PLAY MANAGER **/
    public const int NUM_CHANNELS = 3;
    public const int NUM_PLAYERS = 2;
    public const float AI_INTERVAL = 3.0f;
    public const float WARNING_INTERVAL = 15.0f;
    public const float TICK_INTERVAL = 15.0f;

    /** PLAYER **/
    public const int PLAYER_HEALTH = 200;
    public const int PLAYER_WEALTH = 15;
    // Number of coins to add after coinInterval seconds
    public const int NUM_COINS_TO_ADD = 8;
    public const float COIN_INTERVAL = 1.0f;
    // TURNBASED SETTINGS
    public const int TURNBASED_PLAYER_HEALTH = 250;
    public const int TURNBASED_PLAYER_WEALTH = 10;
    public const int TURNBASED_NUM_COINS_TO_ADD = 15;
    /* these 3 support the 3 different cases
     * Case 1: return to deck and get new cards:
     *          returnCardsToDeck = true, replacePlayedCards = false, numNewCardsToDraw = 5
     * Case 2: replace played cards
     *          returnCardsToDeck = false, replacePlayedCards = true
     * Case 3: draw new cards each tick
     *          returnCardsToDeck = false, replacePlayedCards = false, numNewCardsToDraw = 1
     */
    public const int NUM_NEW_CARDS_TO_DRAW = 5;
    public const bool RETURN_CARDS_TO_DECK = false;
    public const bool REPLACE_PLAYED_CARDS = true;
    public const bool REPLACE_PLAYED_CARD_IMMEDIATE = false;
    // number of cards initially dealt
    public const int NUM_HAND_CARDS = 5;
    public const int NUM_CHANNELCARD_LIMIT = 3;

    /** CHANNELS **/
    public const bool RESET_NEGATIVE_ATTACK = true; // Reset net attack to 0 if it goes -ve with distance penalty
    public const int SYNERGY_ATTACK = 5;
    public const int SYNERGY_HEALTH = 5;

    /** DECK COMPOSISTION **/
    public static Dictionary<int, int> DECK_CATEGORY_COUNT = new Dictionary<int, int>();

    public static List<string> DECK_UNIQUE_STRATEGY_CARDS = new List<string>();

    public static Dictionary<int, string> CARD_CATEGORY_NAMES = new Dictionary<int, string>();

    public static int DECK_MAX_DUPLICATE_COUNT = 100; // Basically none right now

    public static int MAX_DECK_COUNT = 10;

    public const string UNSAVED_DECK_MESSAGE = "You have unsaved changes to your deck! Do you want to discard them?";

    // Based on the fact that many of the cards are 184x184
    // The atlas is always 2048x2048, and 2048/184 = 11.13
    // So in theory we could have 11x11 cards ( 121 ), but cards do vary in size
    // and that can both lead to less space and packing problems, so to be on the
    // safe side we limit the max size to a 100 which should enough leave room 
    public const int MAX_CARDS_PER_DYNAMIC_ATLAS = 100;

    /** INCENTIVES **/
    public const int REWARD_GAME_WIN = 35;
    public const int REWARD_GAME_LOSS = 0;
    public const int REWARD_GAME_DRAW = 0;

    /******* BEGIN CONFIG FILE *******/
    /** REDRAW **/
    public static int COST_FRESH_DRAW = 1;

    public static string WEALTH_TYPE = "Wealth";

    public static float HOVER_INTERVAL = 2.0f;

    /** INVITE ONLY SIGNUP **/
    public static bool INVITE_ONLY_SIGNUP = false;

    public static float PVP_OPPONENT_TIMEOUT = 10.0f;
    public static int AI_MOVE_PROBABILITY = 50;
    /******* END CONFIG FILE *******/

    /* VALIDATOR CREDENTIALS */
    public const string INVITE_CODE_VALIDATOR_EMAIL = "validator@silentlegends.com";
    public const string INVITE_CODE_VALIDATOR_PASSWORD = "nevergonnagiveyouupnevergonnaletyoudown";

    /** ERROR MSGS **/
    public const string INVITE_CODE_INVALID = "Mamma Mia! You entered wrong code. Please try again.";
    public const string INVITE_CODE_USED = "Whoooaaa! Can't use the same code twice! ";
    public const string EMAIL_EXISTING = "You son-of-a-gun, you! That email address is already registered. Please use a different email address.";
    public const string LOGIN_INVALID = "Bummer! You entered wrong Email/Password. Please try again.";
    public const string NETWORK_CONNECTION_ERROR = "Oh, snap!  Internet is down. Please check your connection and try again.";
    public const string GAME_NETWORK_CONNECTION_ERROR = "Rotten luck.  We lost internet connection. Please check your connection and play another game.";
    public const string PARSE_ERROR = "I do not have the foggiest idea what just happened. Please try again.\n";
    public const string GAME_FORFEIT = "Are you sure you want to quit this exciting game?\nPS: We always shed a tear when players quit. :)";
    public const string GAME_OPPONENT_FORFEIT = "Take a bow!! You just won the game. Your opponent quit, rather than keep fighting. :) #winning";
    public const string COINS_INSUFFICIENT = "Blimey! You don't have enough coins at the moment. Play another game and earn more coins.";
    public const string CURRENCY_NOT_AVAILABLE = "Not sure how you managed, but you can't buy this item with that currency!";
}
