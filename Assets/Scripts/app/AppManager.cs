﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.ComponentModel;

public class AppManager : MonoBehaviour {

    void Awake () {
        GA.API.Design.NewEvent("App:Launch");
        GA.API.Design.NewEvent("App:" + Application.platform + ":" + SystemInfo.deviceModel);
        Helper.Log( "Attempting to load the last session ticket" );
        GameManager.AttemptToLoadSessionTicket();
        AudioListener.volume = 0;
    }

    // Use this for initialization
    void Start () {
        if(!GameManager.CONFIG_LOADED) {
            Helper.LoadConfig();
            //Keep the object alive in all scenes
            DontDestroyOnLoad(gameObject);
    
      
        }
    }       
}
