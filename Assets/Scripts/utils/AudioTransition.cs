using UnityEngine;
using System.Collections;

public class AudioTransition : MonoBehaviour {
    public GameObject loader;
    GameObject mainRoot;

    void Start () {
        // Set reference to Main gameobject
        mainRoot = GameObject.Find("Main");

        StartCoroutine(AudioUtils.Fade(audio, FadeType.In, 2));
    }

    public IEnumerator FadeAndLoadNextLevel (AudioSource audio, string levelName) {
        NGUITools.AddChild(mainRoot, loader);
        yield return StartCoroutine(AudioUtils.Fade(audio, FadeType.Out, 1.5f));
        Application.LoadLevel(levelName);
    }

    //    void OnDestroy() {
    //        StartCoroutine(StopAudio()); 
//    ^----- Above THROWS Coroutine couldn't be started because the the game object 'Main' is inactive!
//           UnityEngine.MonoBehaviour:StartCoroutine(IEnumerator)
//        print("Script was destroyed");
//    }
//
//    IEnumerator StopAudio(){
//        yield return StartCoroutine(AudioUtils.Fade(audio, FadeType.Out, 2));
//    }
//    void OnDisable() {
//        print("script was removed");
//    }

    IEnumerator OnApplicationQuit() {
//        if (Application.loadedLevelName.ToLower() != "finalsplash")
        yield return StartCoroutine("DelayedQuit");
        Application.Quit();
//        if (!allowQuitting)
//            Application.CancelQuit();
        
    }
    IEnumerator DelayedQuit() {
//        Application.LoadLevel("finalsplash");
//        yield return new WaitForSeconds(showSplashTimeout);
//        allowQuitting = true;
        // Save any final preferences
        PlayerPrefs.Save();
        GA.API.Design.NewEvent("App:Quit");
        return null;
    }
}

