using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using Serialization;
using PlayFab;
using PlayFab.Json;
using PlayFab.ClientModels;

public class Helper {
public static void ShowMessage (GameObject mainRoot, GameObject msgWindow, string msg) {
        // Show status message
        GameObject newMsgWindowObject = NGUITools.AddChild(mainRoot, msgWindow);
        MsgWindow newMsgWindow = newMsgWindowObject.GetComponentInChildren<MsgWindow>();
        newMsgWindow.ShowMessage(msg);
    }
public static Loading GetLoadingAnimation(GameObject parent, GameObject prefab) {
        GameObject loaderClone = NGUITools.AddChild(parent, prefab);
        Loading loader = loaderClone.GetComponent<Loading>();
        // Set up loader
//        loader.SetLoadingRect(new Rect(300, 150, 60, 60));
        loader.showText = false;
        loader.showBackground = false;
        loader.showSprite = true;
        loader.showLoadingBar = false;
        loader.loadingText = "Loading";
        return loader;
    }
	public static void LoadConfig() {
		if(!GameManager.CONFIG_LOADED) {
            // Get product name
            TextAsset config = (TextAsset)Resources.Load("config", typeof(TextAsset));
//            Helper.Log("config: " + config.text);

            JsonData data = JsonMapper.ToObject(config.text);
            Helper.Log("APP ID: " + data["id"]);
            GameManager.APPID = (string) data["id"];
            GameManager.APPNAME = (string) data["name"];
            // Load properties from config
            Config.COST_FRESH_DRAW = (int) data["config"]["costFreshRedraw"];
            Config.WEALTH_TYPE = (string) data["config"]["wealthType"];
            Config.HOVER_INTERVAL = (float) data["config"]["hoverInterval"];
            Config.INVITE_ONLY_SIGNUP = (bool) data["config"]["inviteOnlySignup"];
            Config.PVP_OPPONENT_TIMEOUT = (float) data["config"]["pvpOpponentTimeout"];
            Config.AI_MOVE_PROBABILITY = (int) data["config"]["aiMoveProbability"];

            // Load the deck builder rules
            Config.DECK_MAX_DUPLICATE_COUNT = (int) data["deck_rules"]["max_duplicate_count"];
            Config.MAX_DECK_COUNT = (int) data["deck_rules"]["max_deck_count"];

            foreach( var entryName in ((IDictionary) data["deck_rules"]["category_names"]).Keys ) {
                var entry = data["deck_rules"]["category_names"][(string) entryName];
                Config.CARD_CATEGORY_NAMES.Add( Int32.Parse( (string) entryName ), (string) entry );
            }

            for( int i = 0; i < data["deck_rules"]["unique_strategy_cards"].Count; i++ ) {
                Config.DECK_UNIQUE_STRATEGY_CARDS.Add( (string) data["deck_rules"]["unique_strategy_cards"][i] );
            }

            foreach( var entryName in ((IDictionary) data["deck_rules"]["category_count"]).Keys ) {
                var entry = data["deck_rules"]["category_count"][(string) entryName];
                Config.DECK_CATEGORY_COUNT.Add( Int32.Parse( (string) entryName ), (int) entry );
            }

            

            GameManager.TUTORIAL_DATA = data["tutorial"];
            GameManager.CONFIG_LOADED = true;
        }
	}

    public static void SetParent(Transform child, Transform parent) {
        child.parent = parent;
        // Hack to work with NGUI scales when parents are changed after the object is added to the scene outside of UIRoot
        child.localScale = Vector3.one;
//        child.position = Vector3.zero;
        child.localPosition = Vector3.zero;
    }

    public static void ShowPlayFabError(PlayFabError playFabError, GameObject mainRoot, GameObject msgWindowPrefab) {
        Helper.Log(playFabError.ToString());
        // TODO: Fancy error messages
        var message = Config.PARSE_ERROR + playFabError.ToString();
        switch(playFabError.Error) {
        case PlayFabErrorCode.UnableToConnectToDatabase:
            message = Config.NETWORK_CONNECTION_ERROR;
            break;
        case PlayFabErrorCode.EmailAddressNotAvailable:
            message = Config.EMAIL_EXISTING;
            break;
        case PlayFabErrorCode.InvalidEmailOrPassword:
        case PlayFabErrorCode.AccountNotFound:
            message = Config.LOGIN_INVALID;
            break;
        case PlayFabErrorCode.InternalServerError:
            message = Config.NETWORK_CONNECTION_ERROR;
            break;
        }
        Helper.ShowMessage(mainRoot, msgWindowPrefab, message);
    }

    public static void Shuffle<T> (List<T> objectList) {
//        Helper.Log("shuffle");
        for (int i=0; i < objectList.Count; ++i) {
            int other = UnityEngine.Random.Range(0, objectList.Count);
            if (other != i) {
                T swap = objectList[i];
                objectList[i] = objectList[other];
                objectList[other] = swap;
            }
        }
    }

    public static void Log(object msg) {
        // Log some debug information only if this is a debug build
        if (Debug.isDebugBuild) {
            Debug.Log(msg);
        }
    }
}
