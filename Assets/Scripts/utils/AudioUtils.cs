﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum FadeType {
    In,
    Out}
;

public class AudioUtils : MonoBehaviour {
    private const float MAX_VOLUME = 0.3F;

    static public IEnumerator Fade (AudioSource audio, FadeType fadeType, float timer) {
        float start = fadeType == FadeType.In ? 0.0F : MAX_VOLUME;
        float end = fadeType == FadeType.In ? MAX_VOLUME : 0.0F;
        float i = 0.0F;
        float step = 1.0F / timer;

        if (audio != null) {
            // Start audio if Fading in
            if (fadeType == FadeType.In) {
                audio.volume = 0;
                audio.Play();
            }
            while (i <= 1.0F) {
                i += step * Time.deltaTime;
                audio.volume = Mathf.Lerp(start, end, i);
                yield return new WaitForSeconds(step * Time.deltaTime);
            }
            if (fadeType == FadeType.Out) {
                // stop the audio from using up resources in the background
                audio.Stop();
            }
        }
    }

    static public IEnumerator CrossFade (AudioSource fadeIn, AudioSource fadeOut, float timer) {
        float start = 0.0F;
        float end = MAX_VOLUME;
        float i = 0.0F;
        float step = 1.0F / timer;
        
        if (fadeIn != null && fadeOut != null) {
            // Start audio for Fadin
            fadeIn.volume = 0;
            fadeIn.Play();
            while (i <= 1.0F) {
                i += step * Time.deltaTime;
                fadeIn.volume = Mathf.Lerp(start, end, i);
                fadeOut.volume = Mathf.Lerp(1 - start, 1 - end, i);
                yield return new WaitForSeconds(step * Time.deltaTime);
            }
            // stop the audio from using up resources in the background
            fadeOut.Stop();
        }
    }

    static public void Play (AudioSource audio) {
        audio.volume = MAX_VOLUME;
        audio.Play();
    }
}
