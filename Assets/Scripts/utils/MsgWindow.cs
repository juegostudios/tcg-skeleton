﻿using UnityEngine;
using System;
using System.Collections;

public class MsgWindow : MonoBehaviour {
    public UILabel msgLabel;
    public string msgText;
    public UIButton button;
    public UILabel btnLabel;

    void Update () {
        msgLabel.text = msgText;
    }

    public void ShowMessage (string msg) {
        msgText = msg;
    }

    public void ShowMessage (string msg, string buttonText) {
        msgText = msg;
        if(String.IsNullOrEmpty(buttonText)) {
            // Hide button
            button.gameObject.SetActive(false);
        } else {
            btnLabel.text = buttonText;
            button.gameObject.SetActive(true);
        }
    }

    public void HideWindow () {
        gameObject.SetActive(false);
    }

    public void CloseWindow () {
        NGUITools.Destroy(gameObject);
    }
}
